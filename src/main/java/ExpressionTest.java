/*
 * Sample file for generating an AST
 */
public class ExpressionTest {
    public static void main(String[] args) {
        int cookie1 = 10;
        int cookie2 = 20;

        int both = cookie1 + cookie2;

        System.out.println(both);

        if(cookie1 > cookie2){
            System.out.println("Cookie 1 > Cookie 2");
        }
        else{
            System.out.println("Cookie 2 > Cookie 1");
        }

    }
}
