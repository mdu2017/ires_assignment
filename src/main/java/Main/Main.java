/*
 * Author: Mark Du
 * Assignment: Evaluations Assignment (BaylorIRES 2020)
 * Assignment Description: Take in a command line argument
 * as a directory and scan through all subdirectories and generate
 * AST files for source code files.
 * Date: 12/12/2019
 */
package Main;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.visitor.TreeVisitor;
import com.github.javaparser.printer.*;

import static java.lang.System.exit;

public class Main {

    //Extensions for file names
    static final String JAVA_EXTENSION = ".java";
    static final String AST_EXTENSION = ".ast";

    //Path for all source files and ast files
    private static List<Path> srcFiles = new ArrayList<>();

    public static void main(String[] args) {
        if(args.length != 1){
            System.out.println("Error, bad argument count");
            exit(1);
        }
        String startDir = args[0];

        //Check directories for source files
        Path startDirectory = Paths.get(startDir);
        try {
            checkDirectory(startDirectory);
        } catch (Exception ex){
            ex.printStackTrace();
        }

        //Generate ast files
        generateASTFiles(srcFiles);
    }

    /**
     * Check current and subdirectories for source files
     * @param startDirectory starting directory
     * @return void
     * @throws IOException File input/output exception
     */
    private static void checkDirectory(Path startDirectory) throws IOException{

        //All paths in current directory
        DirectoryStream<Path> paths = Files.newDirectoryStream(startDirectory);

        //For each directory, recursively scan through all subdirectories and add its path
        paths.forEach(path -> {
            if(Files.isDirectory(path)){
                try {
                    checkDirectory(path);
                } catch(IOException ex){
                    ex.printStackTrace();
                }
            }

            //If a java source file, add path to list of source files
            if(path.toString().endsWith(JAVA_EXTENSION)){
                srcFiles.add(path);
            }
        });
    }

    /**
     * Creates the .ast files
     * @param files the list of files to make for ast
     */
    public static void generateASTFiles(List<Path> files){

        //Create .ast files for each source code file
        files.forEach(path -> {
                try {
                    //Path for .ast file
                    String astPath = path.toString().concat(AST_EXTENSION);

                    //The .ast file to write to
                    FileWriter writer = new FileWriter(new File(astPath));

                    //The root node for the abstract syntax tree
                    CompilationUnit ast = StaticJavaParser.parse(Paths.get(path.toString()));

                    //Dot printer for AST (can be made into a visual graph)
                    DotPrinter dp = new DotPrinter(true);

                    //Visitor for AST traversal
                    TreeVisitor tv = new TreeVisitor() {
                        @Override
                        //Prints the node's content
                        public void process(Node node) {
                            System.out.println(node.toString());
                        }
                    };

                    //Functions to visit nodes of tree
//                    tv.visitPreOrder(ast);
//                    tv.visitPostOrder(ast);
//                    tv.visitLeavesFirst(ast);

                    //Dot printer can provide a visual output for the AST
                    writer.write(dp.output(ast));
                    writer.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
        });
    }
}
