/*
 * File for running the JUnit Tests
 */
package JUnit;

import static org.junit.Assert.*;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.visitor.TreeVisitor;
import org.junit.Test;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class RunnerTest {

    @Test
    public void SampleTest(){
        String sample = "test";

        assertEquals("test", sample);
    }

    @Test
    public void checkTestFile() throws IOException {
        CompilationUnit testNode = StaticJavaParser.parse(Paths.get("src/test/java/JUnit/Test.java"));
        ArrayList<String> test = new ArrayList<>();

        TreeVisitor tv = new TreeVisitor() {
            @Override
            public void process(Node node) {
                test.add(node.toString());
            }
        };
        tv.process(testNode);

        assertNotNull(test);
    }

    @Test
    public void checkDirectories() throws IOException {
        //All paths in current directory
        DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get("src"));

        assertNotNull(paths);
    }
}