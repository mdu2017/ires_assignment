/*
 * Sample file for generating an AST
 */
import java.util.Objects;

/**
 * Sample class
 */
public class Sample {
    private int num;
    private String word;

    public Sample(){
        num = 1;
        word = "hello";
    }

    public Sample(int n, String w){
        this.num = n;
        this.word = w;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "num=" + num +
                ", word='" + word + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sample sample = (Sample) o;
        return num == sample.num &&
                Objects.equals(word, sample.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(num, word);
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
