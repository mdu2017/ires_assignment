# README #

This contains instructions for running the application.

The application will scan through a given directory and all its 
subdirectories and generate an AST and store it in a .ast file 
for all source code files.

### Getting set up ####
* Clone repository to desired location
* Install Maven if not already installed
* Check to make sure Java version is set to SDK 12

### Running the application ###
* How to run the application
	- Navigate to the project folder and run:
	- - mvn clean install
	- - mvn exec:java -Dexec.args="<Start_Directory_Here>"
* How to run JUnit tests
	- Clone the project to your directory of choice
	- Navigate to project directory and run:
	- - mvn clean install
	- - mvn test
	
### Setup in IntelliJ ###
* Open IntelliJ IDEA and import Maven project
* Click on src  -> main -> java -> Main -> right click on Main.java

### Information ###
* Environment
	- IntelliJ IDEA
* Configuration
	- Java SDK 12
* Dependencies
	- JavaParser
	- Java.nio
	- JUnit 4.0

### Contact ###

* mark_du1@baylor.edu